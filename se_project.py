from pyknow import *

type_app_v = '0'
os_v = '0'
hosting_v = '0'
scale_v = '0'
async_v = '0'
compile_v = '0'
time_v = '0'
perf_v = '0'

class Answear(Fact):
    """Info about facts."""
    pass


class WhichEnvironment(KnowledgeEngine):

    # Mobile rules

    @Rule(Answear(type_app='1'), Answear(os='1'), Answear(hosting='1'))
    def mobile1(self):
        print("Java EE / Spring / GCP")

    @Rule(Answear(type_app='1'), Answear(os='1'), Answear(hosting='2'))
    def mobile2(self):
        print("Java EE / Spring / Openstack")

    @Rule(Answear(type_app='1'), Answear(os='1'), Answear(hosting='3'))
    def mobile3(self):
        print("Java EE / Spring / FedoraServer")

    @Rule(Answear(type_app='1'), Answear(os='2'), Answear(hosting='1'))
    def mobile4(self):
        print("Swift/ xcode / GCP")

    @Rule(Answear(type_app='1'), Answear(os='2'), Answear(hosting='2'))
    def mobile5(self):
        print("Swift/ xcode / Openstack")

    @Rule(Answear(type_app='1'), Answear(os='2'), Answear(hosting='3'))
    def mobile6(self):
        print("Swift/ xcode /FedoraServer")

    @Rule(Answear(type_app='1'), Answear(os='3'), Answear(hosting='1'))
    def mobile7(self):
        print("Python 3/ Kivy / GCP")

    @Rule(Answear(type_app='1'), Answear(os='3'), Answear(hosting='2'))
    def mobile8(self):
        print("Python 3/ Kivy / Openstack")

    @Rule(Answear(type_app='1'), Answear(os='3'), Answear(hosting='3'))
    def mobile9(self):
        print("Python 3/ Kivy/ FedoraServer")

    # Web rules

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='1'), Answear(compile='0'), Answear(scale='1'),
              Answear(hosting='1'))
    def web1(self):
        print("GO / IRIS/ GCP")

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='1'), Answear(compile='0'), Answear(scale='1'),
              Answear(hosting='2'))
    def web2(self):
        print("GO / IRIS / Openstack")

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='1'), Answear(compile='0'), Answear(scale='1'),
              Answear(hosting='3'))
    def web3(self):
        print("GO / IRIS / FedoraServer")

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='1'), Answear(compile='0'), Answear(scale='2'),
              Answear(hosting='1'))
    def web4(self):
        print("Python 3/ Django / FedoraServer")

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='1'), Answear(compile='0'), Answear(scale='2'),
              Answear(hosting='2'))
    def web5(self):
        print("Python 3/ Django / Openstack")

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='1'), Answear(compile='0'), Answear(scale='2'),
          Answear(hosting='3'))
    def web6(self):
        print("Python 3/ Django / FedoraServer")

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='2'), Answear(compile='0'), Answear(scale='0'),
              Answear(hosting='1'))
    def web7(self):
        print("PHP / React PHP / GCP")

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='2'), Answear(compile='0'), Answear(scale='0'),
              Answear(hosting='2'))
    def web8(self):
        print("PHP / React PHP / Openstack")

    @Rule(Answear(type_app='2'), Answear(time='1'), Answear(async='2'), Answear(compile='0'), Answear(scale='0'),
              Answear(hosting='3'))
    def web9(self):
        print("PHP / React PHP/ FedoraServer")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='1'), Answear(scale='1'),
              Answear(hosting='1'))
    def web10(self):
        print("Java EE / Spring / GCP")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='1'), Answear(scale='1'),
              Answear(hosting='2'))
    def web11(self):
        print("Java EE / Spring / Openstack")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='1'), Answear(scale='1'),
              Answear(hosting='3'))
    def web12(self):
        print("Java EE / Spring / FedoraServer")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='1'), Answear(scale='2'),
              Answear(hosting='1'))
    def web13(self):
        print("Java SE / Spark / GCP")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='1'), Answear(scale='2'),
              Answear(hosting='2'))
    def web14(self):
        print("Java SE / Spark / Openstack")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='1'), Answear(scale='2'),
              Answear(hosting='3'))
    def web15(self):
        print("Java SE / Spark / FedoraServer")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='2'), Answear(scale='0'),
              Answear(hosting='1'))
    def web16(self):
        print("Java script / Socket IO / GCP")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='2'), Answear(scale='0'),
              Answear(hosting='2'))
    def web17(self):
        print("Java script / Socket IO / Openstack")

    @Rule(Answear(type_app='2'), Answear(time='2'), Answear(async='0'), Answear(compile='2'), Answear(scale='0'),
              Answear(hosting='3'))
    def web18(self):
        print("Java script / Socket IO / FedoraServer")

    # Desktop rules

    @Rule(Answear(type_app='3'), Answear(os='1'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='1'))
    def desktop1(self):
        print("Python 3 / Wx Python / GCP")

    @Rule(Answear(type_app='3'), Answear(os='1'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='2'))
    def desktop2(self):
        print("Python 3 / Wx Python / Openstack")

    @Rule(Answear(type_app='3'), Answear(os='1'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='3'))
    def desktop3(self):
        print("Python 3/ Wx Python/ FedoraServer")

    @Rule(Answear(type_app='3'), Answear(os='2'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='1'))
    def desktop4(self):
        print("Swift / xcode / GCP")

    @Rule(Answear(type_app='3'), Answear(os='2'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='2'))
    def desktop5(self):
        print("Swift / xcode / Openstack")

    @Rule(Answear(type_app='3'), Answear(os='2'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='3'))
    def desktop6(self):
        print("Swift / xcode / FedoraServer")

    @Rule(Answear(type_app='3'), Answear(os='3'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='1'))
    def desktop7(self):
        print("C#/ Forms / MS Azure")

    @Rule(Answear(type_app='3'), Answear(os='3'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='2'))
    def desktop8(self):
        print("C#/ Forms / Openstack")

    @Rule(Answear(type_app='3'), Answear(os='3'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='3'))
    def desktop9(self):
        print("C#/ Forms / Windows Server ")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='1'), Answear(scale='1'), Answear(hosting='1'))
    def desktop10(self):
        print("Java EE / Spring / GCP")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='1'), Answear(scale='1'), Answear(hosting='2'))
    def desktop11(self):
        print("Java EE / Spring / Openstack")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='1'), Answear(scale='1'), Answear(hosting='3'))
    def desktop12(self):
        print("Java EE / Spring / FedoraServer")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='1'), Answear(scale='0'), Answear(hosting='1'))
    def desktop13(self):
        print("Java SE / Spark / GCP")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='1'), Answear(scale='0'), Answear(hosting='2'))
    def desktop14(self):
        print("Java SE / Spark / Openstack")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='1'), Answear(scale='0'), Answear(hosting='3'))
    def desktop15(self):
        print("Java SE / Spark / FedoraServer")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='2'), Answear(scale='0'), Answear(hosting='1'))
    def desktop16(self):
        print("Java Script / Electron / GCP")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='2'), Answear(scale='0'), Answear(hosting='2'))
    def desktop17(self):
        print("Java Script / Electron / Openstack")

    @Rule(Answear(type_app='3'), Answear(os='4'), Answear(compile='2'), Answear(scale='0'), Answear(hosting='3'))
    def desktop18(self):
        print("Java Script / Electron / FedoraServer")

    # All rules

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='1'), Answear(scale='1'), Answear(hosting='1'))
    def all1(self):
        print("Java EE / Spring / GCP")

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='1'), Answear(scale='1'), Answear(hosting='2'))
    def all2(self):
        print("Java EE / Spring / Openstack")

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='1'), Answear(scale='1'), Answear(hosting='3'))
    def all3(self):
        print("Java EE / Spring / FedoraServer")

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='1'), Answear(scale='2'), Answear(hosting='1'))
    def all4(self):
        print("Java SE / Spark / GCP")

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='1'), Answear(scale='2'), Answear(hosting='2'))
    def all5(self):
        print("Java SE / Spark / Openstack")

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='1'), Answear(scale='2'), Answear(hosting='3'))
    def all6(self):
        print("Java SE / Spark / FedoraServer")

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='2'), Answear(scale='0'), Answear(hosting='1'))
    def all7(self):
        print("Java script / Phone GAP / GCP")

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='2'), Answear(scale='0'), Answear(hosting='2'))
    def all8(self):
        print("Java script / Phone GAP / Openstack")

    @Rule(Answear(type_app='4'), Answear(perf='1'), Answear(compile='2'), Answear(scale='0'), Answear(hosting='3'))
    def all9(self):
        print("Java script / Phone GAP / FedoraServer")

    @Rule(Answear(type_app='4'), Answear(perf='2'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='1'))
    def all10(self):
        print("Python 3/ Kivy / GCP")

    @Rule(Answear(type_app='4'), Answear(perf='2'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='2'))
    def all11(self):
        print("Python 3/ Kivy / Openstack")

    @Rule(Answear(type_app='4'), Answear(perf='2'), Answear(compile='0'), Answear(scale='0'), Answear(hosting='3'))
    def all12(self):
        print("Python 3/ Kivy / FedoraServer")


# Menu
type_app_v = input("Jakiego typu aplikacja, 1-Mobilna, 2-Web, 3-Desktop, 4-Wszystkie: ")
if type_app_v == '1':
    os_v = input("Jaki system operacyjny? 1-Android, 2-iOS, 3-Oba: ")
    if os_v == '1' or os_v == '2' or os_v == '3':
        hosting_v = input("Jaki rodzaj hostingu?, 1-Cloud, 2-PrivateCloud, 3-OnPremis: ")
        if hosting_v == '1' or hosting_v == '2' or hosting_v == '3':
            pass
        else:
            print("Wybrano niewłasciwą opcje")
            exit(0)
    else:
        print("Wybrano niewłasciwą opcje")
        exit(0)
elif type_app_v == '2':
    time_v = input("Ile czasu na zbudowanie aplikacji?, 1-Malo, 2-Duzo: ")
    if time_v == '1':
        async_v = input("Aplikacja będzie asynchroniczna?, 1-Tak, 2-Nie: ")
        if async_v == '1':
            scale_v = input("Czy aplikacja ma być łatwo skalowalna?, 1-Tak, 2-Nie: ")
        elif async_v != '1' and async_v == '2':
            pass
        else:
            print("Wybrano niewłasciwą opcje")
            exit(0)
    elif time_v == '2':
        compile_v = input("Aplikacja ma być napisana w jezyku kompilowalnym?  1-Tak, 2-Nie: ")
        if compile_v == '1':
            scale_v = input("Czy aplikacja ma być łatwo skalowalna?, 1-Tak, 2-Nie: ")
        elif compile_v != '1' and compile_v == '2':
            pass
        else:
            print("Wybrano niewłasciwą opcje")
            exit(0)
    else:
        print("Wybrano niewłaściwą opcje")
        exit(0)
    hosting_v = input("Jaki rodzaj hostingu?, 1-Cloud, 2-PrivateCloud, 3-OnPremis: ")
    if hosting_v == '1' or hosting_v == '2' or hosting_v == '3':
        pass
    else:
        print("Wybrano niewłasciwą opcje")
        exit(0)
elif type_app_v == '3':
    os_v = input("Jaki system operacyjny? 1-Linux, 2-Windows, 3-MacOS, 4-Wszystkie: ")
    if os_v == '4':
        compile_v = input("Aplikacja ma być napisana w jezyku kompilowalnym?  1-Tak, 2-Nie: ")
        if compile_v == '1':
            scale_v = input("Czy aplikacja ma być łatwo skalowalna?, 1-Tak, 2-Nie: ")
        else:
            pass
    else:
        pass
    hosting_v = input("Jaki rodzaj hostingu?, 1-Cloud, 2-PrivateCloud, 3-OnPremis: ")
    if hosting_v == '1' or hosting_v == '2' or hosting_v == '3':
        pass
    else:
        print("Wybrano niewłasciwą opcje")
        exit(0)
elif type_app_v == '4':
    perf_v = input("Wydajność aplikacji ma znaczenie? 1-Tak, 2-Nie: ")
    if perf_v == '1':
        compile_v = input("Aplikacja ma być napisana w jezyku kompilowalnym? 1-Tak, 2-Nie: ")
        if compile_v == '1':
            scale_v = input("Czy aplikacja ma być łatwo skalowalna?, 1-Tak, 2-Nie: ")
        elif compile_v != '1' and compile_v == '2':
            pass
        else:
            print("Wybrano niewłasciwą opcje")
            exit(0)
    elif perf_v != '1' and perf_v == '2':
        pass
    else:
        print("Wybrano niewłasciwą opcje")
        exit(0)

    hosting_v = input("Jaki rodzaj hostingu?, 1-Cloud, 2-PrivateCloud, 3-OnPremis: ")
    if hosting_v == '1' or hosting_v == '2' or hosting_v == '3':
        pass
    else:
        print("Wybrano niewłasciwą opcje")
        exit(0)
else:
    print("Wybrano niewłasciwą opcje")
    exit(0)

# SE loop
we = WhichEnvironment()
we.reset()
we.declare(Answear(type_app=type_app_v))
we.declare(Answear(os=os_v))
we.declare(Answear(time=time_v))
we.declare(Answear(async=async_v))
we.declare(Answear(compile=compile_v))
we.declare(Answear(scale=scale_v))
we.declare(Answear(hosting=hosting_v))
we.declare(Answear(perf=perf_v))
print('Odpowiedź:')
we.run()

print(we.facts)